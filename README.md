# pypoc

## Getting started

That is a proof-on-concept for a minimal django test application with a REST interface. So it is a minimal app
with django and rest_framework with a demonstration purpose. Feel free to play with it, and do not trust its security.

You can see here two subdirectories:

* "poc" is the django app.
* "iac" is an ansible script to deploy it to a remote linux as a systemd service.

Accidentally, both "poc" and "iac" are python based, simply because both ansible and django are python-based. So we have
luck and the scripts are quite similar, actually they could run in the same project directory, but I think it is better
if they are separate (the codes itself has nothing to do to the other).
