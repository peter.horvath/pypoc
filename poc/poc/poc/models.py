from django.db import models

class Insurance(models.Model):
    name = models.CharField(max_length=200)
    valid_from = models.DateTimeField('valid from')
    valid_to = models.DateTimeField('valid to')
