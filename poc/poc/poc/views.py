from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics
from . import serializers
from . import models

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

class InsuranceViewSet(viewsets.ModelViewSet):
    queryset = models.Insurance.objects.all()
    serializer_class = serializers.InsuranceSerializer
    permission_classes = [permissions.IsAuthenticated]

class InsuranceList(generics.ListAPIView):
    serializer_class = serializers.InsuranceSerializer
    
    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = models.Insurance.objects.all()
        insurance_name = self.request.query_params.get('name')
        if insurance_name is not None:
            queryset = queryset.filter(name=insurance_name)
        return queryset
