# poc

It is a trivial django web-app, primarily with demonstration purpose. It is also using rest_framework to manage entities
called "Insurace".

How can you use it:

It has the usual project configuration (requirements.txt, intended to create a local virtualenv). So we need a currently
(2023-11-21) uptodate python3, pip and virtualenv modules. And nothing more.

To try it, use the following commands:

* Create the virtual environment with the command ```python3 -m venv pyenv```. That creates the virtual environment in the local "pyenv" directory.
* Activate this virtual environment by the command ```. ./pyenv/bin/activate``` (might be a little bit different on windows, afaik there you have some .cmd or so).
* Upgrade it to the latest pip by the command ```python -m pip install --upgrade pip```.
* Finally, install the project requirementa with the command ```python -m pip install -r requirements.txt```.

You can deactivate the current virtual environment with the ```deactivate``` command any time.

If everything is ok, then now a

```
python poc/manage.py runserver 127.0.0.1:8000
```

will start your development instance, visible on the http://127.0.0.1:8000 URL. If you don't have any security fear, you can also use the URL 0.0.0.0:8000 to make the app worldwide available.

If you have gnu make in the project path, then the

```make dev```

command does all of these at once.

# How to use the deployed system

## How to create "Insurance" entities:

First, use the ```python poc/manage.py shell``` to enter into a django shell. There,

```
from poc.models import Insurance
q = Insurance(name="insurance name 1", valid_from=datetime.datetime.now(), valid_to=datetime.datetime.now())
q.save()
q.id
```

creates the new entity. Note, q.id is empty until you did not save the object (remember Java or "auto_increment").

## How to use the REST API:

To list all Insurance objects, do this:

```
curl -H 'Accept: application/json; indent=4' -u admin:admin http://127.0.0.1:8000/rest/insurances/
```

Only a single insurance instance:

```
curl -H 'Accept: application/json; indent=4' -u admin:admin http://127.0.0.1:8000/rest/insurances/1/
```

A filtering query to find only the insurances named "insurances name 1":

```
curl -H 'Accept: application/json; indent=4' -u admin:admin http://127.0.0.1:8000/insurancename/?name='insurance%20name%201'
```

As you can see, giving any other "name" GET-parameters, the result will be empty with the default database:

```
curl -H 'Accept: application/json; indent=4' -u admin:admin http://127.0.0.1:8000/insurancename/?name=test
```
