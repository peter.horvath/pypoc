The lxc container was generated so:

apt install lxd
lxc init --minimal
lxc launch ubuntu:22.04 first
lxc exec first -- bash -c 'echo ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBfsok9nqxUz0DIDcUpkGMxv/Zj4YJUJy5SSQsDWgDlV maxx@hwen.de >/root/.ssh/authorized_keys'

(comment out pam_logind everywhere in /etc/pam.d, it makes ubuntu in lxc buggy)
